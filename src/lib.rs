pub mod issues;
pub mod models;

use self::issues::core::IssueReqBuilder;

pub static API_V4_PATH: &str = "https://gitlab.com/api/v4";

pub struct ApiClient {
    token: String,
}

impl ApiClient {
    pub fn new(token: &str) -> ApiClient {
        ApiClient {
            token: String::from(token),
        }
    }

    pub fn issue_req(&self) -> IssueReqBuilder {
        IssueReqBuilder::new(&self.token)
    }
}
