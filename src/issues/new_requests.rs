use std::collections::HashMap;
use crate::models::Issue;
use chrono::{DateTime, Utc};
use futures::prelude::*;

pub struct NewIssueReqBuilder {
    token: String,
    id: u32,
    title: String,
    description: Option<String>,
    iid: Option<u32>,
    confidential: Option<bool>,
    assignee_ids: Vec<u32>,
    milestone_id: Option<u32>,
    labels: Vec<String>,
    created_at: Option<DateTime<Utc>>,
    due_date: Option<DateTime<Utc>>,
    merge_request_to_resolve_discussions_of: Option<u32>,
    discussion_to_resolve: Option<String>,
}

impl NewIssueReqBuilder {
    pub fn new(id: u32, title: &str, token: &str) -> NewIssueReqBuilder {
        NewIssueReqBuilder {
            token: String::from(token),
            title: String::from(title),
            description: None,
            iid: None,
            id,
            confidential: None,
            assignee_ids: Vec::new(),
            milestone_id: None,
            labels: Vec::new(),
            created_at: None,
            due_date: None,
            merge_request_to_resolve_discussions_of: None,
            discussion_to_resolve: None,
        }
    }

    pub fn description(&mut self, description: &str) -> &mut Self {
        self.description = Some(String::from(description));
        self
    }

    pub fn iid(&mut self, iid: u32) -> &mut Self {
        self.iid = Some(iid);
        self
    }

    pub fn confidential(&mut self, confidential: bool) -> &mut Self {
        self.confidential = Some(confidential);
        self
    }

    pub fn assignee(&mut self, assignee: u32) -> &mut Self {
        self.assignee_ids.push(assignee);
        self
    }

    pub fn milestone_id(&mut self, milestone_id: u32) -> &mut Self {
        self.milestone_id = Some(milestone_id);
        self
    }

    pub fn label(&mut self, label: &str) -> &mut Self {
        self.labels.push(String::from(label));
        self
    }

    pub fn created_at(&mut self, created_at: DateTime<Utc>) -> &mut Self {
        self.created_at = Some(created_at);
        self
    }

    pub fn due_date(&mut self, due_date: DateTime<Utc>) -> &mut Self {
        self.due_date = Some(due_date);
        self
    }

    pub fn merge_request_to_resolve_discussions_of(&mut self, merge_request_to_resolve_discussions_of: u32) -> &mut Self {
        self.merge_request_to_resolve_discussions_of = Some(merge_request_to_resolve_discussions_of);
        self
    }

    pub fn discussion_to_resolve(&mut self, discussion_to_resolve: &str) -> &mut Self {
        self.discussion_to_resolve = Some(String::from(discussion_to_resolve));
        self
    }

    pub fn send(&self) -> Result<Issue, Box<dyn std::error::Error>> {
        let mut params: HashMap<&str, String> = HashMap::new();
        
        let path = format!("{}/projects/{}/issues", crate::API_V4_PATH, self.id);

        params.insert("title", self.title.clone());

        if let Some(description) = self.description.clone() {
            params.insert("description", description);
        } 
        if let Some(iid) = self.iid {
            params.insert("iid", iid.to_string());
        }
        if let Some(confidential) = self.confidential {
            params.insert("confidential", confidential.to_string());
        }
        if let Some(milestone_id) = self.milestone_id {
            params.insert("milestone_id", milestone_id.to_string());
        }
        if let Some(iid) = self.iid {
            params.insert("iid", iid.to_string());
        }

        for l in &self.labels {
            params.insert("labels", l.to_string());
        }

        // impl labels and assignees

        let client = reqwest::blocking::Client::new();

        Ok(client.post(&path)
            .header("PRIVATE-TOKEN", &self.token)
            .query(&params)
            .send()?
            .json::<Issue>()?)
    }

    pub async fn send_async(&self) -> Result<Issue, reqwest::Error> {
        let mut params: HashMap<&str, String> = HashMap::new();
        
        let path = format!("{}/projects/{}/issues", crate::API_V4_PATH, self.id);

        params.insert("title", self.title.clone());

        if let Some(description) = self.description.clone() {
            params.insert("description", description);
        } 
        if let Some(iid) = self.iid {
            params.insert("iid", iid.to_string());
        }
        if let Some(confidential) = self.confidential {
            params.insert("confidential", confidential.to_string());
        }
        if let Some(milestone_id) = self.milestone_id {
            params.insert("milestone_id", milestone_id.to_string());
        }
        if let Some(iid) = self.iid {
            params.insert("iid", iid.to_string());
        }

        for l in &self.labels {
            params.insert("labels", l.to_string());
        }

        // impl labels and assignees

        let client = reqwest::Client::new();

        future::ok(client.post(&path)
            .header("PRIVATE-TOKEN", &self.token)
            .query(&params)
            .send().await?
            .json::<Issue>().await?).await
    }
}
