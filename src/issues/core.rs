use super::new_requests::NewIssueReqBuilder;

pub struct IssueReqBuilder {
    token: String,
}

impl IssueReqBuilder {
    pub fn new(token: &str) -> IssueReqBuilder {
        IssueReqBuilder {
            token: String::from(token),
        }
    }

    pub fn new_issue(&self, id: u32, titel: &str) -> NewIssueReqBuilder {
        NewIssueReqBuilder::new(id, titel, &self.token)
    }
}