use chrono::{DateTime, Utc};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Issue {
    pub id: u32,
    pub iid: u32,
    pub project_id: u32,
    pub title: String,
    pub description: Option<String>,
    pub state: String,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
    pub closed_at: Option<DateTime<Utc>>,
    pub closed_by: Option<User>,
    pub labels: Vec<String>,
    pub milestone: Option<Milestone>,
    pub assignees: Vec<User>,
    pub author: User,
    pub assignee: Option<User>,
    pub user_notes_count: u32,
    pub merge_requests_count: u32,
    pub upvotes: u32,
    pub downvotes: u32,
    pub due_date: Option<DateTime<Utc>>,
    pub confidential: bool,
    pub discussion_locked: Option<bool>,
    pub web_url: String,
    pub time_stats: TimeStat,
    pub task_completion_status: TaskCompletionStatus,
    pub weight: Option<i32>,
    pub blocking_issues_count: Option<i32>,
    pub has_tasks: bool,
    pub _links: Links,
    pub references: References,
    pub subscribed: bool,
    pub moved_to_id: Option<String>,
    pub health_status: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Milestone {
    pub due_date: Option<DateTime<Utc>>,
    pub project_id: u32,
    pub state: String,
    pub description: Option<String>,
    pub iid: u32,
    pub id: u32,
    pub titel: String,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
    pub closed_at: Option<DateTime<Utc>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct User {
    pub id: u32,
    pub name: String,
    pub username: String,
    pub state: String,
    pub avatar_url: String,
    pub web_url: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TimeStat {
    pub time_estimate: u32,
    pub total_time_spent: u32,
    pub human_time_estimate: Option<String>,
    pub human_total_time_spent: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TaskCompletionStatus {
    pub count: u32,
    pub completed_count: u32,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Links {
    #[serde(alias = "self")]
    pub _self: String,
    pub notes: String,
    pub award_emoji: String,
    pub project: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct References {
    pub short: String,
    pub relative: String,
    pub full: String,
}